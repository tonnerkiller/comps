import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-collections-home',
  templateUrl: './collections-home.component.html',
  styleUrls: ['./collections-home.component.css']
})
export class CollectionsHomeComponent implements OnInit {

  data = [
    { age: 70, svnr: 1234567, name: 'Hans',  job: 'Privatier' },
    { name: 'Günther', age: 21, job: 'Fließenleger' },
    { name: 'Hinnak Hieronymus', age: 16, job: 'Azubi' }
  ];

  headers = [
    { key: 'name', label: 'Vorname'},
    { key: 'age', label: 'Alter'},
    { key: 'job', label: 'erlernter Beruf'},
  ];
  
  constructor() { }

  ngOnInit() {
  }

}
